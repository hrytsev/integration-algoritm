function calculateRectangleMethod() {
    let step = (1 - 0) / 5;
    let sum = 0;

    for (let i = 0; i < 5; i++) {
        let x = 0.1 + i * step;
        sum += Math.pow(x, 2) * Math.atan(x);
    }

    let result = 0.2 * sum;
    return result;
}

function calculateTrapezoidalMethod() {
    let step = (1 - 0) / 5;
    let sum = 0;

    sum += Math.pow(0, 2) * Math.atan(0); // f(a)
    sum += Math.pow(1, 2) * Math.atan(1); // f(b)

    for (let i = 1; i < 5; i++) {
        let x = 0.1 + i * step;
        sum += 2 * Math.pow(x, 2) * Math.atan(x);
    }

    let result = 0.5 * step * sum;
    return result;
}

function calculateSimpsonsMethod() {
    let step = (1 - 0) / 5;
    let sum1 = 0;
    let sum2 = 0;

    for (let i = 1; i < 5; i++) {
        let x = 0.1 + i * step;
        if (i % 2 === 0) {
            sum1 += Math.pow(x, 2) * Math.atan(x);
        } else {
            sum2 += Math.pow(x, 2) * Math.atan(x);
        }
    }

    let result = (step / 3) * (Math.pow(0.1, 2) * Math.atan(0.1) + Math.pow(1, 2) * Math.atan(1) + 4 * sum1 + 2 * sum2);
    return result;
}

document.getElementById("rectangleMethodResult").innerText = calculateRectangleMethod();
document.getElementById("trapezoidalMethodResult").innerText = calculateTrapezoidalMethod();
document.getElementById("simpsonsMethodResult").innerText = calculateSimpsonsMethod();